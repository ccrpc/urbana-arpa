---
title: Allocation
draft: false
menu: main
weight: 60
---

The Mayor and Urbana City Council have reviewed the applications submitted for ARPA funding. After reviewing project applications, presentations, federal regulations, and the City of Urbana’s ARPA Concept Plan, ***25 projects*** have been selected to receive funds. This allocation ***awards $9,949,343*** in total.
Upon the City's official adoption of a project list, the City and sub-recipients will enter into contracts and intergovernmental agreements, with finalized dates, funding timelines, and project-specific terms and conditions.

Of the applications submitted for Urbana ARPA funds, 46 met the minimum qualifications—41 from outside entities, requesting $35.2 million and
5 projects submitted by the City, requesting $3.8 million. All applicants were invited to give project presentations to the City Council. City projects were presented on [December 12, 2022](https://urbanaillinois.us/node/10431) and external agency applications presented at a special City Council meeting held over the following three days ([12/13](https://urbanaillinois.us/node/10449), [12/14](https://urbanaillinois.us/node/10450), and [12/15](https://urbanaillinois.us/node/10451)).
The Mayor and City Council held a series of discussions ([12/19](https://urbanaillinois.us/node/10432), [1/9](https://urbanaillinois.us/node/10478), [1/17](https://urbanaillinois.us/node/10478), [1/24](https://urbanaillinois.us/node/10479), [2/6](https://www.urbanaillinois.us/node/10571), and [2/13](https://www.urbanaillinois.us/node/10572)) to review the
submitted materials, reviewer scores, presentations, and how to best fulfil to goals of the Concept Plan. During these discussions, City Council first came to a general consensus on projects that most merited ARPA funding. Tentative allocation amounts for these projects were discussed until a general consensus for a funding level was achieved.

## Project List and Funding Amounts

**Figure 1** below shows the project names and awarded funding amount. In total, $9,949,343 is allocated to 25 projects. On average, each project was funded at 53% of amount that they requested in their initial application.

<rpc-table url="project_list.csv"
  table-title="Figure 1 - Project List and Funding Amount"
  text-alignment="c,c"></rpc-table>

The selected projects address every [funding goal from the Concept Plan](https://ccrpc.gitlab.io/urbana-arpa/concept/afgs/). **Figure 2** below shows a general estimate for the amount of funding allocated to each goal. For the table below, projects could be included multiple times, if they significantly addressed multiple goals. Many of the projects addressed multiple goals, both directly and indirectly. While the type of
projects, request size, and quality of applications substantially varied, the distribution of awarded funds is generally
proportional to the total amount of funds requested for each goal by all projects. 


<rpc-table url="funding_by_category.csv"
  table-title="Figure 2 - Distribution of Funding by Concept Plan Goal (Projects can be included more than once)"
  text-alignment="l,r"></rpc-table>

**Significantly Address was generally defined as a major component of the ARPA request, proportional to the size of the request. Many organizations and projects included multiple goals to varying degrees, and the goals themselves have broad definitions. There is no single correct way to assign goal eligibility. Since projects could be included multiple times, the total figure exceeds $9,949,343.*
