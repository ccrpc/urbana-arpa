---
title: "Application"
draft: false
menu: main
weight: 50
---
<br>

### The application period for Urbana ARPA funds has closed! If you wish to see the information and resources that were available during the application period (Friday, September 16th, 2022 to Wednesday, November 16th, 2022), these are still available below. 
<br>

____

The city is seeking applications for organizations that will provide services meeting the [Urbana ARPA Funding Goals](https://ccrpc.gitlab.io/urbana-arpa/concept/afgs/). Rather than households or businesses seeking individual assistance, applicants should be organizations who will use the funds to facilitate one of the funding goals. 

The list of application questions and the rubric that will be used to assess applications are found below, for reference.

- [Application Content](./ARPA_Application.pdf)
- [Application Rubric](./ARPA_Application_Rubric.pdf)

See the **Questions and Assistance** section below for further guidance, or to get in touch with RPC staff for assistance in the application process. 

## Questions and Assistance



<iframe width="640" height="360" src="https://www.youtube.com/embed/iztjmPtBWM8" title="ARPA Tutorial" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

**See the embedded video above for a walkthrough of the Urbana ARPA website and application process!**

Any questions about the application can be directed to - 

J.D. McClanahan, Planner II – Champaign County Regional Planning Commission<br>
jmcclanahan@ccrpc.org<br>
217-328-3313, ext. 196

Open office hours will also be held to offer in-person application assistance. These office hours will be held in the second floor study rooms at the Urbana Free Library (210 W Green St), on the dates below -   

- Tuesday, September 27 - 2-4PM
- Tuesday, October 11 - 2-4PM
- Tuesday, October 25 - 2-4PM
- Tuesday, November 8 - 2-4PM

Additionally, applicants may schedule video or phone calls for application assistance, using the calendar below.

<br>

<!-- Calendly inline widget begin -->
<div class="calendly-inline-widget" data-url="https://calendly.com/jdrpc/arpa" style="min-width:320px;height:630px;"></div>
<script type="text/javascript" src="https://assets.calendly.com/assets/external/widget.js" async></script>
<!-- Calendly inline widget end -->

<br>

## Frequently Asked Questions

*As applicants reach out for help, this section will be populated with common questions and answers, to provide a resource for all applicants. The Treasury Department has put together their own [ARPA SLFRF FAQs](https://home.treasury.gov/system/files/136/SLFRF-Final-Rule-FAQ.pdf) document that applicants may also find useful.* 

**What are the restrictions on ARPA spending?**

On the Urbana ARPA website, the [Federal ARPA Regulations](https://ccrpc.gitlab.io/urbana-arpa/concept/regs/) page provides a broad overview of the federal rules guiding ARPA spending, including the categories of eligible and ineligible uses. Further information can be found on the Treasury Department's [website about ARPA state and Local Funds](https://home.treasury.gov/policy-issues/coronavirus/assistance-for-state-local-and-tribal-governments/state-and-local-fiscal-recovery-funds). Resources on this site include the [full final rule text](https://www.govinfo.gov/content/pkg/FR-2022-01-27/pdf/2022-00292.pdf), a [shorter overview of the final rule](https://home.treasury.gov/system/files/136/SLFRF-Final-Rule-Overview.pdf), and an [FAQ document](https://home.treasury.gov/system/files/136/SLFRF-Final-Rule-FAQ.pdf) about the rules.

In addition to the ARPA-specific rules, [Section 13](https://home.treasury.gov/system/files/136/SLFRF-Final-Rule-FAQ.pdf#page=48) of the FAQ document makes clear that funding recipients are required to comply with the Uniform Guidance for federal grants. This means that standard federal procurement requirements (for example, competitive bids for expenditures above a certain amount) apply to ARPA funding recipients.

**What are the ongoing reporting requirements for these funds?**

The primary, ongoing reporting responsibilities of all Urbana ARPA subrecipients are quarterly updates on **1)** the amount of ARPA funds spent, and **2)** the project status (Not Started/Completed less than 50 percent/Completed 50 percent or more/Completed). There are additional answer fields in the required quarterly reports, as described on the ARPA Compliance and Reporting Guidance page [linked here](https://home.treasury.gov/system/files/136/SLFRF-Compliance-and-Reporting-Guidance.pdf#page=22), but most of these fields (like project descriptions, award amount, project location, etc.) will stay the same quarter-to-quarter. 

Projects in the Public Health and Negative Economic Impact spending categories (which will likely include most of the applicants for Urbana ARPA funding) will also be required to describe the [Impacted or Disproportionately Impacted populations](https://home.treasury.gov/system/files/136/SLFRF-Compliance-and-Reporting-Guidance.pdf#page=20) served by their project. As mentioned in the previous paragraph, these demographics should be constant over the life of the project and should not require quarterly updates. Additional reporting requirements, based on the specific project type, are described [later in the reporting document](https://home.treasury.gov/system/files/136/SLFRF-Compliance-and-Reporting-Guidance.pdf#page=27). 

As described in the previous question about ARPA restrictions, recipients will be required to comply with the [Uniform Guidance](https://www.grants.gov/learn-grants/grant-policies/omb-uniform-guidance-2014.html) for federal awards and should maintain records to demonstrate compliance with these rules, but this will not be part of the ongoing reporting to the City of Urbana. As part of these standard federal rules, recipients who receive more than $750,000 in federal funds in a single year will be required to perform the federal [Single Audit](https://home.treasury.gov/system/files/136/SLFRF-Compliance-and-Reporting-Guidance.pdf#page=12). 

Urbana's application for ARPA funding also asks applicants to describe how they will measure the success of their ARPA-funded programming. Applicants should be prepared to provide data on these standards of success, as well as narrative descriptions of their ARPA-funded progress, on an annual basis and upon completion of their project. 

**Can ARPA funds be combined with other funds?**

Generally, yes. As described in the Treasury Department's [ARPA FAQ document](https://home.treasury.gov/system/files/136/SLFRF-Final-Rule-FAQ.pdf#page=32&zoom=100,93,428), ARPA funds can be combined with other funding sources, as long as the ARPA money is being spent on an ARPA-eligible project and is not violating any restrictions that are in place from the other funding sources.

**Are for-profit businesses eligible to apply for these funds?**

In general, the City of Urbana is not seeking to provide individual business assistance with these funds. Individual business owners seeking assistance should seek out funding opportunities from local economic development agencies and organizations. These economic development agencies and organizations are eligible to apply for ARPA funding for business support programs, which they are then responsible for reallocating in order to provide individual assistance.

However, for-profit companies are eligible to apply for ARPA funding if they are seeking to provide a specific program that meets another [Urbana ARPA Funding Goal](https://ccrpc.gitlab.io/urbana-arpa/concept/afgs/). For example, a for-profit company could apply for ARPA funds to construct affordable low-income housing, increase the availability of affordable food to vulnerable community members, or increase access to broadband internet for low-income households. If a prospective applicant is in doubt about their project eligibility, they should reach out to Urbana ARPA support staff using one of the contact points listed above on this page.

**Can ARPA funding be used on affordable housing projects?**

Yes! The Treasury Department has put out [additional guidance](https://home.treasury.gov/system/files/136/Affordable-Housing-How-To-Guide.pdf) that specifically describes how ARPA funds can be used in the construction of affordable housing, including how they can be combined with other funding sources and how to know if a given housing project qualifies for ARPA funds.

**What are the long-term rules for assets (equipment, facilities, etc.) purchased with ARPA funds?**

[Question 13.16](https://home.treasury.gov/system/files/136/SLFRF-Final-Rule-FAQ.pdf#page=55) of the Treasury Department's ARPA FAQ document answers this question in detail. In general, ARPA-purchased assets can change in use from their originally-stated purpose, as long as their ongoing use is still consistent with an [ARPA-eligible activity](https://home.treasury.gov/system/files/136/SLFRF-Compliance-and-Reporting-Guidance.pdf#page=42). For example, an ARPA-funded facility could change in use from providing homeless services to providing food programs. If ARPA recipients are trying to sell, transfer ownership, or otherwise dispose of assets purchased with ARPA funding, Treasury points to the [Uniform Guidance for Post Federal Award Requirements](https://www.ecfr.gov/current/title-2/subtitle-A/chapter-II/part-200/subpart-D/subject-group-ECFR8feb98c2e3e5ad2?toc=1) for regulations on how to go about disposing of these assets. At that point, recipients should contact the Treasury department for guidance and approval to transfer ownership of these assets.

In addition to the federal regulations, the contracts that recipients sign with the City of Urbana will include specific language on the City's expectations for how ARPA-funded assets will be used. This language will vary depending on the type of asset and individual project.