---
title: Home
draft: false
bannerHeading: "American Rescue Plan Act Funds, City of Urbana"
bannerText: >
  
---

The American Rescue Plan Act (ARPA) was passed in March of 2021 to provide relief to the ongoing effects of the COVID-19 pandemic. One significant component of this law was the Coronavirus State and Local Fiscal Recovery Funds (SLFRF) program, which allocated relief funds to state, local, and tribal governments.

In the words of the [Treasury Department](https://home.treasury.gov/policy-issues/coronavirus/assistance-for-state-local-and-tribal-governments/state-and-local-fiscal-recovery-funds) - 

*The SLFRF program provides governments across the country with the resources needed to:*

- *Fight the pandemic and support families and businesses struggling with its public health and economic impacts*
- *Maintain vital public services, even amid declines in revenue resulting from the crisis*
- *Build a strong, resilient, and equitable recovery by making investments that support long-term growth and opportunity*

As a result of this program, the City of Urbana has been allocated **$12.97 million** to address the impacts of the pandemic. The city has contracted with [Champaign County Regional Planning Commission (RPC)](https://ccrpc.org/) to perform the planning and administration of these funds. This process has **three primary steps—performing background research and developing a list of priorities, developing a concept plan for distributing this money, and implementing the plan.**

The purpose of this website is to

- Keep residents informed on the allocation process for these funds
- Allow residents to provide feedback on where ARPA funds should go 
- Provide a place where eligible local entities can easily apply for part of this funding, once the city's priorities are set

{{<image src="CCRPC_220405_ARPA_ListeningSession_sm.jpg"
  alt="Graphic summary of in-person listening session."
  caption="Graphic summary of in-person listening session. "
  position="right">}}

The [Process](https://ccrpc.gitlab.io/urbana-arpa/process/) page provides a brief summary of the City of Urbana's process for allocating ARPA funds, while the [Concept Plan](https://ccrpc.gitlab.io/urbana-arpa/concept/intro/) provides extensive information on the city's ARPA priorities, what the federal ARPA program is, existing local context that informed the allocation process, public outreach and engagement to deterimine the priorities. After the Concept Plan was approved in late summer of 2022, the funding [Application](https://ccrpc.gitlab.io/urbana-arpa/apply/) opened for a two-month span from September to November 2022, during which 48 funding applications were submitted. Now that all applications have been received, the [Allocation](https://ccrpc.gitlab.io/urbana-arpa/allocation/) page will provide updates on how ARPA funds are dispersed.







