---
title: "Process"
draft: false
menu: main
weight: 20
---

Below, you will find the tentative schedule by which the Urbana ARPA planning process is operating - 


*Fall 2021 – Spring 2022* <br/>
**Background Research and Synthesis of Priorities**
- Compile information on ARPA regulations, existing city priorities, and gaps in existing priorities
- Collect additional input from Urbana residents to refine ARPA priorities
- Set up website for ongoing ARPA documentation
- Work with City to finalize priorities eligible for ARPA funding

*Spring 2022 – Fall 2022* <br/>
**Develop Concept Plan** 
- Develop overall strategy and goals consistent with city goals and federal guidance
- Define metrics to measure progress toward goals and evaluate applications for funding
- Draft a Concept Plan that will serve as a comprehensive guide to the City's ARPA strategy, goals, and application process
- Finalize the Concept Plan with the City

*Fall 2022 – Spring 2027* <br/>
**Implement Plan**
- Outreach, education, and applicant recruitment
- Create and maintain online application forms
- Work with City to process applications, choose recipients, and document process
- Fulfill ongoing reporting requirements of ARPA with Department of Treasury