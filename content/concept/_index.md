---
title: "Concept Plan"
draft: false
menu: main
weight: 40
---

This concept plan has been drafted to guide how the City of Urbana spends its allocation of American Rescue Plan (ARPA) State and Local Fiscal Recovery Funds (SLFRF). The sections of the Concept Plan can be found in the links below, or in the Concept Plan dropdown in the menu at the top of this page.


