---
title: "Urbana ARPA - Public Engagement and Feedback"
draft: false
weight: 17
---
Clear communication and engagement between staff, elected officials, and the public is an essential part of the ARPA allocation process. This page provides information on this communication and engagment, including - 
- Presentations to Urbana City Council
- Explanations of all of the public engagement approaches used for this process
- The qualitative and quantitative results of this public engagement
- The demographics of respondents to the engagement process

## Council Presentations 
All ARPA-related presentations of RPC staff to the Urbana City Council can be found below. This includes meeting recordings, as well as slides or other supplemental materials from these presentations.

### June 2022 - Proposed Urbana ARPA Funding Goals Presentation

Below, you will find a link to the meeting recording where RPC staff presented the proposed Urbana ARPA Funding Goals, based on analyzing public feedback and city priorities. 

[City Council Meeting - Proposed Urbana ARPA Funding Goals](https://www.city.urbana.il.us/Scripts/CouncilVideo/Video.asp?v=/_Video/City_Council/2022/20220621)

Below, you can access the materials presented to the council.

- [City of Urbana ARPA Proposed Funding Goals Presentation](./6_21_22_arpa_urbana_council_presentation_slides.pdf)


### May 2022 - Public Input Summary Presentation

Below, you will find a link to the meeting recording where RPC staff presented a
summary of the ARPA-related public input received in March and April.

[City Council Meeting - ARPA Public Input Review](https://www.city.urbana.il.us/Scripts/CouncilVideo/Video.asp?v=/_Video/City_Council/2022/20220516)

Below, you can access the materials presented to the council and a full list of
the comments received through the online survey.

- [City of Urbana ARPA Public Input Presentation](./5_16_22_urbana_arpa_presentation_slides.pdf)
- [City of Urbana ARPA Public Survey Input](./URARPAsurvey_Q3_Q4_Q5_responses_nonames.pdf)

### April 2022 - ARPA Standard Allowance 

Below, you will find a link to the meeting recording where the council approved
a resolution to allocate $2.5 million of the city's ARPA funds (Standard
Allowance) for lost public sector revenue.

[City Council Meeting - Allocation of the ARPA Standard Allowance](https://www.city.urbana.il.us/Scripts/CouncilVideo/Video.asp?v=/_Video/City_Council/2022/20220425)

### February 2022 - Initial Funding Allocations 

Below, you will find a link to the meeting recording where Mayor Marlin
presented a plan for allocating half of the city's ARPA funds for project
administration, internal city services and infrastructure, and new Urbana Park
District facilities.

[City Council Meeting - Initial Allocations](https://www.city.urbana.il.us/Scripts/CouncilVideo/Video.asp?v=/_Video/City_Council/2022/20220228)

### January 2022 - Planning Process Presentation

Below, you will find a link to the meeting recording where RPC staff presented
an overview of the planning process as well as a summary of existing city
priorities.

[City Council Meeting - ARPA Planning Process](https://www.city.urbana.il.us/Scripts/CouncilVideo/Video.asp?v=/_Video/City_Council/2022/20220124)

Below, you can access the materials presented to the council.

- [City of Urbana ARPA Planning Presentation](./1_24_presentation_slides.pdf)
- [Supplementary Slides - ARPA Federal Regulations](./1_24_supplemental_rules.pdf)
- [Supplementary Slides - ARPA and Urbana's Socioeconomic Background](./1_24_supplemental_local_data.pdf)






## Outreach Strategies

In order to gain public feedback and understand community members’ priorities, RPC staff created an online survey on Survey Monkey, as well as an informational website for the entire ARPA process. This survey and website information and a request to distribute was emailed to 26 community institutions and organizations (public agencies, neighborhood groups, and nonprofits working in Urbana), emailed to 26 places of worship (all located in Urbana, with publicly-listed email addresses), and posted on the city’s website and Facebook page.

In addition, printed versions of this survey were made available at public survey boxes at the Urbana Free Library, Urbana City Hall, Cunningham Township Office, and the offices of the Aspen Court apartment complex (upon request from a community member). These paper surveys were also distributed to visitors during food bank distribution hours at the Stone Creek Food Pantry, St. Vincent DePaul-Urbana, and The Vineyard Food Pantry.

The online and paper surveys were made available in [English](./UrbanaARPAsurvey_english_web.pdf), [Spanish](./UrbanaARPAsurvey_spanish_web.pdf),
[French](./UrbanaARPAsurvey_french_web.pdf), and [Chinese](./UrbanaARPAsurvey_chinese_web.pdf) from March 14th - April 14th.  

## Public Feedback Venues

From this outreach, the public survey received 546 responses—437 via the web survey and 109 via the paper survey.

Forty-two instances of verbal feedback were also received at advertised events, listed below.
- 7 City Council meetings, including four meetings with dedicated ARPA input time: 3/14, 3/21, 3/28, 4/4
  - 21 public commenters
- 1 [Public Listening Session](https://www.city.urbana.il.us/Scripts/CouncilVideo/Video.asp?v=/_Video/City_Council/2022/20220405): 4/5
  - 21 public commenters
  - This event took place at Urbana City Council Chambers (400 S. Vine St.), and
provided an opportunity for members of the public to discuss their thoughts on
the Urbana's ARPA funding with city staff and with one another. 
  - City staff facilitated the conversation and were assisted by a graphic
facilitator, David Michael Moore, who helped visualize and synthesize the
public's comments. The discussions from this meeting, in addition to survey
responses and other public comments, went into the record of ARPA responses for
the council to consider when designating this funding.

{{<image src="listening_session_audience.jpg"
  alt="Members of public providing feedback during listening session"
  caption="Members of public taking part in listening session"
  position="center">}}

{{<image src="graphic_recording.jpg"
  alt="Scan of image showing ideas for how ARPA funds should be spent"
  caption="Final result of listening session and graphic recording"
  position="center">}}

Finally, eight additional instances of feedback were received through ARPA-related emails, letters, or phone calls, and recorded by RPC staff.
Taken together, these instances of feedback total 596 responses, which contained more than a thousand unique ideas for how to spend ARPA funds.

<rpc-chart url="input.csv"
  columns="1,2"
  chart-title="Total Respondents: 596"
  type="doughnut"></rpc-chart>



## Response Content

The first question—and only required question—of the survey asked respondents to rank the five priority areas identified in the [Existing City Priorities page](https://ccrpc.gitlab.io/urbana-arpa/concept/priority_areas), with 1 as the highest priority and 5 as the lowest. The average rankings can be seen below, with Public Health and Safety as the highest priority and Economic Recovery and Development as the lowest, but less than a point difference between the highest and lowest averages, indicating support for all five priorities.

 <rpc-chart url="ranking.csv"
  chart-title="Ranking of Priority Areas"
  type="horizontalBar"
  y-label="Priority Area"
  x-label="Average Score"
  x-min="0"
  x-max="5"></rpc-chart>

In addition, the graph below demonstrates the overall average rankings, compared to the average ranking for specific subpopulations of respondents, providing a means to analyze the priorities of populations who may be prioritized for demonstrated vulnerability and need.

  <rpc-chart url="ranking_all.csv"
  chart-title="Ranking of Priority Areas - Households Making Less than $50,000 Annually"
  type="horizontalBar"
  y-label="Priority Area"
  x-label="Average Score"
  x-min="0"
  x-max="5"></rpc-chart>

The next question asked *“What specific goal(s) or outcome(s) would you like to see achieved with this money?”*. Forty-three percent of survey respondents answered this question, with the following as major repeated goals across the priority areas - 
- This money should be invested in the people that need it most and/or were most impacted by the pandemic
- This money should make progress toward greater equity
- Everyone should have opportunities to meet their basic needs - housing, food, work, safety
- This money should have long-term impacts that increase future stability for the entire community

Finally, the survey asked *“Do you have specific ideas for funding projects and programs related to any of the priority areas?”* and *“Do you have specific ideas for funding projects and programs that don't fit within the priority areas listed above?”*. Forty-nine percent of all respondents provided ideas to these questions (including through in-person or verbal comments). The graph below shows the number of suggestions, by priority area, with Public Health and Safety receiving the most ideas for how to spend the funds.

 <rpc-chart url="comment_count.csv"
  chart-title="Number of Suggested Funding Ideas, by Theme"
  type="horizontalBar"
  x-label="Number of Responses"
  legend=false
  x-min="0"></rpc-chart>



## Short Answer Responses, By Theme

Based on the responses discussed above, the following section will sort the short answer responses by priority area, providing greater insight into the specific goals and ideas put forth by the public. Individual examples of quotes are not seen here, for brevity’s sake, but can be seen in the May presentation to the Urbana City council, [here](https://ccrpc.gitlab.io/urbana-arpa/concept/public_feedback/5_16_22_urbana_arpa_presentation_slides.pdf).

### Sustainable Infrastructure 

Within the priority area of Sustainable Infrastructure, the following overarching goals were commonly discussed - 
- **Transportation** - Improve the quality and safety of the current system for vehicles (improve pavement quality), bikes (more bike facilities and slower vehicle traffic), pedestrians (improve existing sidewalks and fill gaps), and transit (more routes).
- **Utilities** - Invest in renewable energy to reduce greenhouse gas emissions and reduce utility bills.
- **Future preparedness** - Invest in infrastructure that will increase community resilience and safety in anticipation of climate change as well as future emergencies and/or pandemics.

The following quotes are illustrative of public feedback in this category - 
  - “Anyone from 8 to 80 should feel comfortable walking, biking, or finding the bus.”
  - “Better roads for cars and safer streets for pedestrians & bicycles.”
  - “Use funds to decarbonize energy - saves people money and creates better health outcomes.” 
  - “Enable community-wide broadband access that is AFFORDABLE.”
  - “ ‘Harden’ public infrastructure so that buildings can continue to be used safely by both staff and the public in future pandemics and current endemics.” 
  - “Spend on traditional infrastructure aimed at improved sustainability across the city.”
  - “A healthier, more sustainable community that holistically mitigates the impacts of climate change.”

Finally, the graph below shows the repeated ideas for spending in this category. Within the “Other” classification, proposals included snow removal, Amtrak, affordability, connectivity, and incentives for active transportation.

 <rpc-chart url="sustainability_transpo.csv"
  chart-title="Sustainable Infrastructure - Ideas related to Transportation"
  type="horizontalBar"
  x-label="Number of Responses"
  legend=false
  x-min="0"
  wrap=35></rpc-chart>

   <rpc-chart url="sustainability_utilities.csv"
  chart-title="Sustainable Infrastructure - Ideas related to Utilities"
  type="horizontalBar"
  x-label="Number of Responses"
  legend=false
  x-min="0"
  x-max=60
  wrap=35></rpc-chart>

### Public Health and Safety

Within the priority area of Public Health and Safety, the following overarching goals were commonly discussed - 
- **Recreation** - Improve the accessibility of public recreations space, both indoors and outdoors for all ages, with an emphasis on youth-centered spaces and programming.
- **Safety and Law Enforcement** - Reduce crime and all forms of violence in the community. Improve the relationship between law enforcement and residents through increased transparency and trust.
- **Healthcare** - All residents should have access to healthcare and health education as well as resources to stay healthy during this and future pandemics.

The following quotes are illustrative of public feedback in this category -
- “Urbana needs more outdoor public spaces that are accessible and 
- physically engaging for people of all ages.”
- “Urbana has a real need for more indoor public recreation space.  
- Get youth off the streets and away from guns.”
- “Helping our community come together after years of learning and working remotely.” 
- “Public Safety is first and foremost. Without a peaceful and safe society our economy and all other areas will suffer.”
- “Add social workers/counselors as first responders in appropriate situations.”
- “Support police, fire, and public works so they have the best tools available to do their jobs.”
- “I’d like to see our success defined by measurable drops in the rate of gun violence, evictions, incarceration, school violence, truancy, DV cases, unemployment, and police calls.”
- “Reduce inequities in wealth and health outcomes.”

Finally, the graphs below shows the repeated ideas for spending in this category.

 <rpc-chart url="health_rec.csv"
  chart-title="Public Health and Safety - Ideas related to Public Recreation Spaces and Activities"
  type="horizontalBar"
  x-label="Number of Responses"
  legend=false
  x-min="0"
  x-max=55
  wrap=40></rpc-chart>

   <rpc-chart url="health_safety.csv"
  chart-title="Public Health and Safety - Ideas related to Safety and Law Enforcement"
  type="horizontalBar"
  x-label="Number of Responses"
  legend=false
  x-min=0
  x-max=55
  wrap=40></rpc-chart>

   <rpc-chart url="health_public.csv"
  chart-title="Public Health and Safety - Ideas related to Public Health"
  type="horizontalBar"
  x-label="Number of Responses"
  legend=false
  x-min="0"
  x-max=55
  wrap=40></rpc-chart>


### Economic Recovery and Development

Within the priority area of Economic Recovery and Development, the following overarching goals were commonly discussed - 
- **For Individuals and Employees** - Increase and/or subsidize access to basic resources that facilitate employment such as job training and placement services, childcare, and transportation.
- **For Businesses** - Provide support for those hit hardest during the pandemic, particularly small local businesses. In addition, increase support for startups and aspiring entrepreneurs. 
- **For the City** - Invest in downtown development and activity.

The following quotes are illustrative of public feedback in this category - 
- “More vocational training made available to young people who are not 
- college-bound.” 
- “Support for childcare and employee re-training is really critical given past two years as school closures working parents have limited options for child care.”
- “Unaffordable childcare and transportation access are barriers to marginalized community members, including immigrants.” 
- “Revitalization of the downtown area to bring more money to Urbana so we can sustain on our own and continue to provide for our residents for years to come!” 
- “Ofrecer apoyo economico a personas que desean iniciar un negocio. Ofrecer talleres para futuros dueños de negocios. > Offer economic support to people who want to start a business. Offer workshops for future business owners.”
- “Helping households catch up so they have extra money to spend in the community.”
- “A reduction in poverty will lead to a reduction in crime.”

Finally, the graphs below shows the repeated ideas for spending in this category. Within the “Provide Extra Support to Underrepresented Business Owners” classification, targeted groups included women, people of color, LGBTQIA+, and hiring a director of Diversity for Economic Development and Contracting.

 <rpc-chart url="econ_individuals.csv"
  chart-title="Economic Recovery and Development - Ideas related to Individuals and Employees"
  type="horizontalBar"
  x-label="Number of Responses"
  legend=false
  x-min="0"
  wrap=40></rpc-chart>

 <rpc-chart url="econ_owners.csv"
  chart-title="Economic Recovery and Development - Ideas related to Businesses"
  type="horizontalBar"
  x-label="Number of Responses"
  legend=false
  x-min="0"
  x-max=50
  wrap=40></rpc-chart>

### Adequate and Affordable Housing

Within the priority area of Adequate and Affordable Housing, the following overarching goals were commonly discussed - 
- Reduce housing costs for those that need it most, taking measures to prevent homelessness and evictions. This includes permanent housing as well as emergency shelters and transitional housing.
- Support diverse neighborhoods with equitable access to transportation and other amenities, flexible (re)development opportunities, and attention to history.

The following quotes are illustrative of public feedback in this category -
- “I want to see all homeless youth have the opportunity for housing.”
- “More of the excellent use of infill opportunities like on Vine between 
- Green and Elm.”
- “Helping developers who are willing to build more affordable housing but 
- may find the cost/benefit detrimental without funds to help.”
- “Change regulations to allow Accessory Dwelling Units, and increase types of “missing middle housing” like duplexes, triplexes, fourplexes, and allow them in more zoning districts.”
- “I would like to see a more economically diverse Urbana, with money invested in local neighborhoods that have higher need.” 
- “Less violence and crime - I think this is more likely if people are adequately housed and have appropriate social services.”

Finally, the graphs below shows the repeated ideas for spending in this category. In addition, the second graph further breaks down the final category of “Neighborhood Quality.”

 <rpc-chart url="housing.csv"
  chart-title="Adequate and Affordable Housing Ideas"
  type="horizontalBar"
  x-label="Number of Responses"
  legend=false
  x-min="0"
  wrap=50></rpc-chart>

<rpc-chart url="housing_neighborhood.csv"
  chart-title="Adequate and Affordable Housing Ideas - Neighborhood Quality (detail)"
  type="horizontalBar"
  x-label="Number of Responses"
  legend=false
  stacked="true"
  x-min="0"
  x-max=35
  wrap=50></rpc-chart>


### Human Rights and Social Services

Within the priority area of Human Rights and Social Services, the following overarching goals were commonly discussed - 
- Provide supportive services to aid in pandemic recovery and daily stability for those in the community that need it most. 
- Increase the availability and affordability of mental health services. 
- Ensure all residents have convenient access to food.

The following quotes are illustrative of public feedback in this category -
- “Improve the availability of mental health services and trauma-informed 
- programs and outreach for low-income households.”
- “Increased mental health service including first responders, long term 
- outpatient services and residential options.”
- “Affordable, accessible mental health care for trauma victims, including 
- bilingual care for immigrants, would address a real need.”
- “We have several food deserts in Urbana and are in need of permanent food pantries so people don’t have to take several buses to get food.”
- “I hope that those who are most in need in our community will receive the greatest benefit.  The African American community that lives north of University Avenue is one of those groups.”
- “I would like this money to create long-term improvements for the workers and demographic groups worst affected by wealth inequality and the pandemic.”

Finally, the graph below shows the repeated ideas for spending in this category.

 <rpc-chart url="human_rights.csv"
  chart-title="Human Rights and Social Services Ideas"
  type="horizontalBar"
  x-label="Number of Responses"
  legend=false
  x-min="0"
  wrap=40></rpc-chart>

## Urbana ARPA Funding Goals

As discussed in more detail on the [ARPA Funding Goals page](https://ccrpc.gitlab.io/urbana-arpa/concept/afgs/), analyzing the responses above and synthesizing the most prevalent suggestions resulted in the following ARPA Funding Goals for the City of Urbana.

***Public Health and Safety***

1.	Improve accessibility of public recreation space and youth programming.

2.	Increase support for community violence interventions.

***Adequate and Affordable Housing***

3.	Reduce housing costs for those that need it most.

***Human Rights and Social Services***

4.	Increase availability and affordability of mental health services.

5.	Increase availability and affordability of food.

***Economic Recovery and Development***

6.	Increase job training and placement opportunities.

7.	Provide relief and support for local businesses.

***Sustainable Infrastructure***

8.	Invest in infrastructure to increase community health, safety, and future resilience.

## Response Demographics

The following section provides the voluntary demographic data of respondents to the Urbana ARPA survey.

While RPC staff worked to reach a diverse group of respondents, response demographics nonetheless differed from Urbana’s overall population. One likely explanation for gaps in this survey is the lack of targeted focus on university students, contributing to a response base that was older, whiter, and wealthier than the City of Urbana’s student-heavy population. Even with these caveats, the survey captured a large and diverse sample of resident perspectives.

- **What race/ethnic group(s) do you most identify with? (select all that apply)**
  - 85% of survey respondents answered

  <rpc-chart url="race.csv"
  chart-title="Race and Ethnicity of Respondents"
  type="horizontalBar"
  stacked="true"
  y-label="Race or Ethnicity"
  x-label="Percent of Respondents"></rpc-chart>
  
- **What is your age?**
  - 89% of survey respondents answered

<rpc-chart
url="age.csv"
type="horizontalBar"
stacked="true"
x-label="Percent of Population"
x-min="0"
x-max="100"
legend-position="top"
legend="true"
tooltip-intersect="true"
source=" U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B01001"
source-url="https://data.census.gov/cedsci/table?text=B01001&g=0500000US17019_1500000US170190056004,170190057011,170190058004,170190058005,170190060001_1600000US1777005&tid=ACSDT5Y2019.B01001&hidePreview=false"
chart-title="Age Distribution"></rpc-chart>
 

- **What is your household’s approximate annual income?**
  - 84% of survey respondents answered

    <rpc-chart url="income.csv"
  chart-title="Household Income"
  type="horizontalBar"
  stacked="true"
  y-label="Household Income"
  x-label="Percent of Respondents"
  x-min="0"
  x-max="100"></rpc-chart>


- **Gender: How do you identify?**
  - 89% of survey respondents answered

<rpc-chart url="gender.csv"
  chart-title="Gender of Respondents"
  type="horizontalBar"
  y-label="Gender"
  x-label="Percent of Respondents"
  x-min="0"
  x-max="100"></rpc-chart>

- **Employment - Which category(ies) best describe you? (select all the apply)**
  - 87% of survey respondents answered


<rpc-chart url="work.csv"
  chart-title="Employment Status of Respondents"
  type="horizontalBar"
  y-label="Employment Status"
  x-label="Percent of Respondents"
  x-min="0"
  x-max="100"></rpc-chart>


- **What is your relationship to the City of Urbana?  (Select all that apply)**
  - 95% of survey respondents answered
  - 12% selected “Other” alone or in combination with another option 
  - Only 8% (44 people) selected  “Other” alone

<rpc-chart url="urbana.csv"
  chart-title="Relationship to City of Urbana"
  type="horizontalBar"
  y-label="Relationship"
  x-label="Percent of Respondents"
  x-min="0"
  x-max="100"></rpc-chart>

  <rpc-chart url="other_responses.csv"
  chart-title="Relationship to City of Urbana - Other"
  type="doughnut"
  y-label="Relationship"
  x-label="Percent of Respondents"
  x-min="0"
  x-max="100"></rpc-chart>

A geographic analysis of respondent addresses shows that responses were fairly well distributed across the city, with concentrations in the central and southern portions of town.

- **What two streets intersect closest to your home or business in Urbana?**
  - 73% of survey respondents answered

{{<image src="revised dot map_4.png"
  alt="Map showing nearest intersection of respondent homes in Urbana"
  position="center">}}