---
title: "Introduction"
draft: false
weight: 10
---

## ARPA Management for the City of Urbana

This concept plan was drafted to guide how the City of Urbana spends its allocation of American Rescue Plan (ARPA) State and Local Fiscal Recovery Funds (SLFRF).

Urbana was allocated $12,974,560 from the ARPA SLFRF program. Of this total, $415,180 have been allocated to Champaign County Regional Planning Commission (RPC) to administer the ARPA funds. The city also allocated $2.5 million of these funds for Revenue Replacement. The total allocations, along with the more detailed Revenue Replacement allocations, can be seen in the tables below.

<rpc-table url="overall_budget.csv"
  table-title="Urbana ARPA Allocations"
  text-alignment="l,r"></rpc-table>

<rpc-table url="revenue_replacement.csv"
  table-title="Revenue Replacement Allocations"
  text-alignment="l,r"></rpc-table>

With Administration and Revenue Replacement allocations taken into account, this document will guide how the city spends the ***approximately $10 million remaining in ARPA funding, available for city council-selected priorities and projects.***

The remaining sections on this page—Urbana ARPA Best Practices and Application Process—will describe the guiding principles for Urbana ARPA spending and the process through which applicants will request Urbana ARPA funds. The following pages in the Concept Plan provide the additional background of accumulated information and decisions that resulted in final application process. These additional Concept Plan chapters are – 

- Urbana ARPA - Funding Goals
- Urbana ARPA - Public Engagement and Feedback
- Federal ARPA Regulations
- Existing Urbana Data and Demographics
- Existing City Priorities


## Urbana ARPA Best Practices

The following list of best practices was developed by city officials and staff to help guide Urbana’s ARPA spending.

1.	 ARPA/Coronavirus State and Local Fiscal Recovery Funds are temporary and non-recurring. 

2.	Recovery Funds are particularly well-suited for one-time investment in critical public infrastructure projects and long-term assets that will provide measurable benefits over many years.  Future operation and maintenance costs associated with new projects must be taken into account, if applicable. 

3.	 It is important to identify service and recovery gaps where funding is most needed and to prioritize historically underserved, marginalized, or disproportionately-impacted groups and sectors of the economy. 

4.	Allocation of Recovery Funds should reflect community input,  along with priorities identified in the  City Council Strategic Goals 2022-23,  current City of Urbana projects and initiatives,  the ongoing Imagine Urbana  Comprehensive Plan process, and  other plans developed by the City of Urbana.  

5.	Programs, services, and interventions (internal or community-based) supported by Recovery Funds should be evidence-based or be structured in such a way that they can build evidence through program evaluation
6.	New or expanded programs, services, and interventions (internal or community-based) supported by Recovery Funds that will require an ongoing financial commitment after the initial investment should include a proposed sustainability plan and funding source(s).  

7.	The City should strive to leverage Recovery Funds by collaborating with other local city, county, and community partners.  The City should identify other potential sources of state and federal funding to help achieve goals.    These “ARPA scanning and partnering efforts” can be used to enhance and broaden the impact of Recovery Funds across the community

## Application Process

After the Concept Plan is approved by the Urbana City Council, the online [Application](https://ccrpc.gitlab.io/urbana-arpa/apply/) will open, allowing applicants to request Urbana ARPA funding for programs that align with the [Federal ARPA Regulations](https://ccrpc.gitlab.io/urbana-arpa/concept/regs/) and [Urbana ARPA Funding Goals](https://ccrpc.gitlab.io/urbana-arpa/concept/afgs/).

The draft application questions and assessment rubric are found below, for those interested in the content of the future application and the means of analyzing applicants.

- [DRAFT Application Content](./ARPA_Application_DRAFT.pdf)
- [DRAFT Application Rubric](./ARPA_Application_Rubric_DRAFT.pdf)

The application is anticipated to open in Fall of 2022, at which point applicants will be able to submit their proposals. The City of Urbana expects to leave the application open for 60 days, after which time all applications will be assessed using the above rubric (depending on whether all funds are allocated from this intitial application period, the city may re-open the application at a later date, but this is not guaranteed).

The city is seeking applications for organizations that will provide services meeting the [Urbana ARPA Funding Goals](https://ccrpc.gitlab.io/urbana-arpa/concept/afgs/). Rather than households or businesses seeking individual assistance, applicants should be organizations who will use the funds to facilitate one of the funding goals. This means that households applying for rent assistance or businesses applying for help covering their expenses would not qualify for this funding. However, organizations offering to administer programs for individual housing assistance or small business assistance would qualify for funding. The city is not instituting a minimum or maximum required funding amount, and funding can cover a variety of costs to meet the goals, including facility investments, personnel, direct assistance to community members, internal capacity building, and administrative costs.

During the application period, Urbana and RPC staff will provide informational material and contact information to the public, to ensure that all interested applicants are able to navigate the application process and address any questions or concerns that arise. In addition to providing contact information and assistance, staff will also set up designated times in the application period during which prospective applicants can discuss their questions and work through concerns with staff. The application will also be publicized in the community at this time, including through local media and social media, to ensure that all interested parties are aware of the opportunity.  

Following the closing of the application and the scoring of all applicants, the Urbana City Council will provide time during public meetings in which applicants are able to present their proposal to the council and answer any questions that elected officials may have. Following this presentation meeting, city council will hold a later meeting to vote on which applicants will be funded and at what amount.

Further details, including exact dates for the application window and relevant city council meetings, will be published as this information becomes available.