---
title: "Existing Urbana Data and Demographics"
draft: false
weight: 50
---

In line with Urbana’s and the Treasury Department’s focus on equity and direct impacts, the following section will present information on demographic and geographic conditions in the city of Urbana. This will help guide the areas and communities where ARPA spending may be most appropriate.

Unless otherwise specified, data is from the 2015-2019 American Community Survey.

## Poverty 

Urbana has a higher family poverty rate—at 11.8%—than both the state of Illinois and the country at large. Looking specifically at family poverty, with a family described by the Census Bureau as “a group of two people or more (one of whom is the householder) related by birth, marriage, or adoption and residing together” reduces the skew in poverty rates created in university communities like Urbana. In university communities, student households with low or no income either because of unreported family support or simply due to their temporary student condition. These households can sway the overall poverty rate, and in the map, would lead to the conclusion that student neighborhoods are those most in need. For this reason, the graph and map below present family poverty, rather than overall poverty (for the record, the overall individual poverty rate for Urbana is 29.8%, more than double the state and country, at 12.5% and 13.4%, respectively). The map shows the highest rates of family poverty in the area north of University, east of I-74, and west of Cunningham; south of the intersection of Lincoln and University, and in Orchard Downs.

<rpc-chart url="poverty.csv"
  chart-title="Percent of Families in Poverty"
  type="horizontalBar"
  x-label="Percent"
  x-min=0
  legend=false
  colors='midnightblue'>
  </rpc-chart>

{{<image src="pov_bg.png"
  alt="Map showing famliy poverty in Urbana by census block group"
  position="center">}}

## Household Income

Median household income data provides a different perspective than the previous section and reflects some of the same concerns section—household incomes are less than 60% of state and county incomes, and the areas with the lowest median income center on areas of high student housing concentration near Lincoln Avenue. However, other low-income areas are seen as well, including along Cunningham Avenue, west of Martin Luther King Jr Elementary School, and along Colorado Avenue and East Washington Street.

<rpc-chart url="income.csv"
  chart-title="Median Household Income"
  type="horizontalBar"
  x-label="Dollars"
  x-min=0
  legend=false
  colors='midnightblue'>
  </rpc-chart>

{{<image src="mhhi_bg.png"
  alt="Map showing median household income in Urbana by census block group"
  position="center">}}

## Home Value 

Urbana also has a significantly lower median home value than Illinois and the US, with the lowest-valued areas centered on Cunningham Avenue,  between Crystal Lake and Douglas Parks, and along East Washington Street.

<rpc-chart url="mhv.csv"
  chart-title="Median Home Value (Owner-Occupied Homes)"
  type="horizontalBar"
  x-label="Dollars"
  x-min=0
  legend=false
  colors='midnightblue'>
  </rpc-chart>

{{<image src="mhv_bg.png"
  alt="Map showing median home value in Urbana by census block group"
  position="center">}}

## Age Distribution

The distribution of age groups within a population can provide useful insight, especially for analyzing those populations, like youths and seniors, who may be more vulnerable to social, economic, or public health instability. Unsurprisingly, Urbana’s population is made up of significantly more (by roughly three times) college students than Illinois or the US, with correspondingly lower youth, working age, and senior populations. 

Urbana does have a higher proportion of the working-age population to seniors and youths, with roughly 1.8 working-age residents for each youth or senior, compared to 1.3 for both Illinois and the US. 

Looking geographically, the areas of greatest youth concentration are focused on the Lierman neighborhood, west of the intersection of University and I-74, and around Crystal Lake Park. The greatest senior concentrations are found north of East Washington (particularly the block group that contains University Rehabilitation Center [formerly Champaign County Nursing Home] and few other residences), northwest of the intersection of Florida and Vine, as well as block groups encompassing both the city and outlying areas to the east and south.

<rpc-chart url="age.csv"
  chart-title="Age Distribution of Urbana Residents"
  type="horizontalBar"
  x-label="Percent"
  x-min=0>
  </rpc-chart>

{{<image src="youth_bg.png"
  alt="Map showing percent of residents under 18 in Urbana by census block group"
  position="center">}}

{{<image src="senior_bg.png"
  alt="Map showing percent of residents over 65 in Urbana by census block group"
  position="center">}}

## Race and Ethnicity

Urbana has a lower percentage of Non-Hispanic white residents than Illinois or the US, with significantly more Asian residents, slightly more Black and mixed race residents, comparable Native American residents, and significantly fewer Hispanic or Latino residents. At the neighborhood scale, the lowest concentration of non-Hispanic white residents is found in the Ellis subdivision east of Douglas Park, with low percentages seen throughout the northwest and southwest areas of the city, between Colorado and Mumford, and in the Lierman neighborhood.

<rpc-chart url="race.csv"
  chart-title="Race and Ethnicity of Residents"
  type="horizontalBar"
  x-label="Percent"
  x-min=0>
  </rpc-chart>

{{<image src="nhw_bg.png"
  alt="Map showing percent of residents identifying as non-Hispanic white in Urbana by census block group"
  position="center">}}

## Types of Households

As would be expected from its college town context, the city of Urbana has a significantly higher rate of single-person and non-family households than the state or nation. This is paired with a corresponding significantly lower rate of married couple households, and a relatively similar (although lower) rate of single-parent households. Single-parent households are concentrated in the Ellis subdivision, around Crystal Lake Park, and west of the intersection of University and I-74.

<rpc-chart url="household_comp.csv"
  chart-title="Composition of Households"
  type="horizontalBar"
  x-label="Percent"
  x-min=0>
  </rpc-chart>

{{<image src="sphh_bg.png"
  alt="Map showing percent of households led by single parents in Urbana by census block group"
  position="center">}}


## Violent Crime

From 2016 to 2020, Urbana had a lower homicide rate than both Illinois and the US. A geographic analysis of violent violent crimes (here defined as Battery, Assault, Unlawful Restraint, Robbery, Murder, Sexual Assault, Kidnapping, and Aggravated Sexual Abuse) in the community shows that these crimes are concentrated in the Lierman neighborhood, between Colorado and Mumford, west of Philo, and south and east of Crystal Lake Park.

<rpc-chart url="homicide.csv"
  chart-title="2016-2020 Average Annual Homicide Rate"
  type="horizontalBar"
  x-label="Homicides per 100,000 residents"
  x-min=0
  legend=false
  source= "FBI Crime Data Explorer, Urbana Open Data Portal"
  colors='midnightblue'>
  </rpc-chart>

{{<image src="violent_crime.png"
  alt="Map showing violent crimes since 2010 in Urbana by census block group"
  position="center">}}

## Social Vulnerability Index

The [Social Vulnerability Index]( https://www.atsdr.cdc.gov/placeandhealth/svi/index.html) (SVI) is a measurement created by the CDC to help prioritize resources in the event of a disaster. In their words, “a number of factors, including poverty, lack of access to transportation, and crowded housing may weaken a community’s ability to prevent human suffering and financial loss in a disaster. These factors are known as social vulnerability.” Because of this “the CDC/ATSDR Social Vulnerability Index (CDC/ATSDR SVI) uses 15 U.S. census variables to help local officials identify communities that may need support before, during, or after disasters.” The measurements used to create this index are shown in the graphic from the CDC below. The map below features the CDC’s Social Vulnerability Index for the City of Urbana, which is available at the census tract level. This shows the area of highest vulnerability west of I-74 and University and along Cunningham Avenue. 

{{<image src="svi variables.png"
  alt="Table showing the variables used to compute the CDC's Social Vulnerability Index"
  caption="Source - Centers for Disease Control"
  position="center">}}

{{<image src="svi.png"
  alt="Map showing tract-level social vulnerability in Urbana"
  position="center">}}

In order to achieve a more precise, neighborhood-level look at social vulnerability, RPC staff also created an SVI-inspired index for census block groups, the geographic level used for the other analysis seen on this page. Each of the variables used to calculate SVI is not available for the block group; however, the following available variables were used to create a similar analysis.

-	Below Poverty
-	Income 
-	Aged 65 or Older
-	Aged 17 or Younger
-	Single-Parent households 
-	Non-Hispanic White Population
-	Violent Crime

The results of this index can be seen in the map below, with the most vulnerable neighborhoods annotated. 

  {{<image src="bg_svi_annotated.png"
  alt="Map showing block-group level social vulnerability in Urbana, with high-vulnerability neighborhoods annotated"
  position="center">}}

## Qualified Census Tracts

Another geographic classification to note in this discussion is Qualified Census Tracts (QCTs). These are a designation used in existing federal programs to identify low-income areas, containing “50 percent of households with incomes below 60 percent of the Area Median Gross Income (AMGI) or hav[ing] a poverty rate of 25 percent or more” (see [here](https://www.huduser.gov/portal/datasets/qct.html) for more information).

In ARPA, local governments are given more flexibility in their funds when they are spent in QCTs, or on populations otherwise shown to be disproportionately impacted by the pandemic (see the COVID-19 Public Health and Economic Impact section in Eligible uses for more information on these regulations). Largely due to poverty rate impacts of student populations discussed at the top of this page, the Qualified Census tracts in Urbana include every tract on the western edge of the city, along with the tract west of I-74 and University. As seen from presenting the social vulnerability and QCT maps together, I-74 and University, and the northwest and southwest sides of town are consistently recognized as appropriate for additional resources in these maps.

{{<image src="qcts annotated.png"
  alt="Map showing Qualified Census Tracts in Urbana"
  position="center">}}

  {{<image src="bg_svi_annotated.png"
  alt="Map showing block-group level social vulnerability in Urbana, with high-vulnerability neighborhoods annotated"
  position="center">}}

