---
title: "Urbana ARPA - Funding Goals"
draft: false
weight: 15
---

Over the course of the ARPA research and public outreach process, RPC staff developed—and council approved—five ARPA Priority Areas and eight ARPA Funding Goals (AFGs) for the City of Urbana. These AFGs are listed below and sorted by their overarching Priority Areas – 

***Public Health and Safety***

1.	Improve accessibility of public recreation space and youth programming.

2.	Increase support for community violence interventions.

***Adequate and Affordable Housing***

3.	Reduce housing costs for those that need it most.

***Human Rights and Social Services***

4.	Increase availability and affordability of mental health services.

5.	Increase availability and affordability of food.

***Economic Recovery and Development***

6.	Increase job training and placement opportunities.

7.	Provide relief and support for local businesses.

***Sustainable Infrastructure***

8.	Invest in infrastructure to increase community health, safety, and future resilience.

The city is seeking applications for organizations that will provide services meeting the funding goals. Rather than households or businesses seeking individual assistance, applicants should be organizations who will use the funds to facilitate one of the funding goals. This means that households applying for rent assistance or businesses applying for help covering their expenses would not qualify for this funding. However, organizations offering to administer programs for individual housing assistance or small business assistance would qualify for funding. The city is not instituting a minimum or maximum required funding amount, and funding can cover a variety of costs to meet the goals, including facility investments, personnel, direct assistance to community members, internal capacity building, and administrative costs.

The remainder of this page will describe these eight ARPA Funding Goals. For a more detailed rationale of these funding goals, see RPC’s June presentation to City Council [here](https://ccrpc.gitlab.io/urbana-arpa/concept/public_feedback/6_21_22_arpa_urbana_council_presentation_slides.pdf). For a detailed description of the research and outreach process, please see the remaining sections of the Concept Plan (in the menu on the top left of this page, or under the Concept Plan dropdown above).  Finally, to learn more about Urbana’s ARPA application and allocation process, see the Application page. 

## Public Health and Safety

**1.	Improve accessibility of public recreation space and youth programming.**

**2.	Increase support for community violence interventions.**

Within the Public Health and Safety category, 95% of all public suggestions for ARPA spending input focused on recreation or public safety and law enforcement. These priorities also aligned with Mayor-Council goals like “fund and support community partners that promote health and wellness,” “pursue methods to mitigate community violence,” and “enhance and expand public safety resources.”

Similar local investments include the City of Champaign allocating $1,265,354 of their ARPA funds for Neighborhood Ambassadors to address safety and wellness concerns at a neighborhood level and $7,956,144 in ARPA funds for a Community Gun Violence Reduction Blueprint.

## Adequate and Affordable Housing

**3.	Reduce housing costs for those that need it most.**

When the public provided suggestions for how ARPA funding should be spent on Adequate and Affordable Housing, 67% of all responses involved reducing housing costs for those that need it most and taking measures to prevent homelessness and evictions. This builds upon the Mayor and Council’s Strategic Goals for 2022 and 2023, which include “support[ing] housing security and equity” and “improv[ing] housing quality”

Similar local ARPA spending includes $150,000 from Champaign County for C-U at Home for Winter Homeless Services, $187,000 from Champaign County to assist negatively impacted residential water and/or wastewater user accounts, and approximately $2,600,000 allocated by the City of Champaign for housing and homelessness services, including an intergovernmental agreement with the City of Champaign Township for the creation of a year-round, low barrier emergency shelter and additional rent assistance to prevent homelessness.

## Human Rights and Social Services

**4.	Increase availability and affordability of mental health services.**

**5.	Increase availability and affordability of food.**

Fifty-eight percent of all ARPA public input in the category of Human Rights and Social Services covered the need to provide supportive services to aid in pandemic recovery and daily stability for those in the community that need it most, primarily regarding mental health and food. This spending aligns with Mayor-Council Strategic Goals, including “fund and support community partners that promote health and wellness” and “create an incentive package and recruit a new grocery store and essential services to northwest Urbana.”

Examples of similar local ARPA spending include $770,436 from Champaign County to the Champaign County Mental Health Board for the provision of mental health and substance abuse disorder treatment services.

## Economic Recovery and Development

**6.	Increase job training and placement opportunities.**

**7.	Provide relief and support for local businesses.**

All public input in the category of Economic Recovery and Development was focused on supporting employees and/or businesses in some way, whether increasing access to employment resources like job training, childcare, and transportation or providing support for the businesses hit hardest during the pandemic, particularly small local businesses and increasing support for startups and aspiring entrepreneurs. These funding areas also align with Mayor-Council Strategic Goals including “promote workforce development” and “support current local businesses.”

## Sustainable Infrastructure

**8.	Invest in infrastructure to increase community health, safety, and future resilience.**

Fifty-three percent of public ARPA suggestions in the category of Sustainable Infrastructure focused on non-transportation infrastructure, including investing in renewable energy to reduce greenhouse gas emissions and reduce utility bills and preparing for future resiliency in anticipation of climate change as well as future emergencies or pandemics. This feedback also aligns with Mayor-Council Strategic goals including “improve quality of current infrastructure assets,” “increase investment in infrastructure equity,” and “expand Green infrastructure within the community.”

Similar local ARPA investments include up to $500,000 designated by Champaign County to the Sangamon Valley Public Water District for completion of water main extension design work for the northernmost portion of its service area in Champaign County, up to $500,000 from Champaign County to the Prairie Research Institute to conduct geophysical mapping of the Mahomet Aquifer to help determine the sustainability of the water resource, and $7,772,988 from the City of Champaign for Garden Hills lighting and $5,000,000 for Garden Hills stormwater infrastructure.

It should be noted that the infrastructure-specific ARPA spending categories include only investments in water, sewer, or broadband infrastructure. Other infrastructure investments could be made eligible under the Revenue Replacement category (a route that the City of Urbana has elected not to take), or through the COVID-19 Public Health and Economic Impact spending categories, by justifying the expenses as related to specific pandemic needs or specific negative economic impacts for disproportionately impacted populations.
 

