---
title: "Existing City Priorities"
draft: false
weight: 60
---

In developing a list of city priorities, a range of officially adopted documents from several decades of Urbana history were consulted, looking both at long-term goals and more immediate needs reflected in the recent Mayor-Council goals and city budget. As evident in their name, recovery is a central theme for the ARPA State and Local Fiscal Recovery Funds (SLFRF). Helping local governments recover from the pandemic is a primary goal of this program. Because of this, older and long-range plans may not fully capture the priorities for ARPA spending, but looking at both long-running and more recent needs can establish helpful context to guide the allocation of these funds.

The plans consulted for this analysis are listed below - 

- [2022-2023 Mayor-Council Goals](https://urbanaillinois.us/mayor-council-strategic-goals)
- 2021 [Imagine Urbana Comprehensive Plan](https://imagineurbana.com/) Public Input
- [2021-2022 Budget](https://www.urbanaillinois.us/departments/finance/financial-reports/annual-budget)
- [2021 Capital Improvement Plan](https://urbanaillinois.us/departments/public-works/about-public-works/engineering/capital-improvement-plan/capital-improvement)
- [2020-2024 Urbana HOME Consortium Consolidated Plan](https://www.urbanaillinois.us/government/community-development/grants-management/consolidated-plan#:~:text=The%20City%20of%20Urbana%20and,in%20the%20Consortium%2Dwide%20area.)
- [2020 Pedestrian Master Plan](https://ccrpc.org/transportation/urbana_bicycle_and_pedestrian_plans/2020_urbana_pedestrian_master_plan_(final_report).php)
- [2020 Park District Strategic Plan](https://www.urbanaparks.org/get-involved/strategic-planning/)
- [2020 Urbana Kickapoo Rail Trail Extension Study](https://cms3.revize.com/revize/champaigncountyrpc/Document%20Center/Divisions/Planning%20And%20Development/Project%20&%20Documents/Transportation/Urbana-Kickapoo-Rail-Trail-Extension-Study-Final-Report-December-2020.pdf)
- [2016 Bicycle Master Plan](https://www.urbanaillinois.us/bicycle-master-plan)
- [2015-2020 Climate Action Plan](https://www.urbanaillinois.us/sites/default/files/attachments/ucap-p2.pdf)
- [2015 Multi-Jurisdictional Hazard Mitigation Plan](https://ccrpc.org/planning/champaign_county_multi-jurisdictional_hazard_mitigation_plan.php)
- [2013-2020 Sustainable Water Management Plan](https://www.urbanaillinois.us/sites/default/files/attachments/swmp-draft9-reduced-size.pdf)
- [2012 Downtown Urbana Plan](http://www.urbanaillinois.us/downtown)
- [2008 Boneyard Creek Master Plan](https://www.urbanaillinois.us/departments/public-works/about-public-works/engineering/boneyard-creek-master-plan)
- [2005 Comprehensive Plan](https://www.urbanaillinois.us/sites/default/files/attachments/Comprehensive_Plan.pdf)
- [1998 Historic Preservation Plan](https://www.urbanaillinois.us/sites/default/files/attachments/Historic%20Preservation%20Plan%20-%201998.pdf)

Based on the existing plans and documents examined for this process, the following six categories were identified as priority areas for City of Urbana investment.

- Sustainable Infrastructure
- Public Health and Safety
- Economic Recovery and Development
- Adequate and Affordable Housing
- Human Rights and Social Services
- *Public Spaces and Resources*

The first four of these priority areas are also the main themes in the [2022-2023 Mayor-Council Strategic Goals](https://ccrpc.gitlab.io/urbana-arpa/concept/strategic_goals/), while the final two were themes that also were prevalent in the analyzed documents. 

When the Urbana City Council was presented with these categories and the rest of the information featured on this page before ARPA public outreach began, councilmembers elected to consolidate the list, removing “Public Spaces and Resources” as a separate category for future outreach and sorting. 

The remainder of this page discussed these five ARPA priority areas, their occurence in existing plans and documents, as well as ways that these themes have appeared in previous local investments and in other communities' use of ARPA funds. In the text below, individual quotes from the Imagine Urbana process, examples of previous local investment,  and individual case studies from other communities were removed and summarized, for brevity’s sake. To see these examples in more detail, please see the January 2022 presentation to the city council, [here](https://ccrpc.gitlab.io/urbana-arpa/concept/public_feedback/1_24_presentation_slides.pdf).

## Sustainable Infrastructure

Sustainable Infrastructure refers to physical infrastructure (including water, transportation, electricity, and broadband internet) that is adequate and resilient for current needs and future changes, including population growth, climate change, and new technologies. Investments using ARPA funds could include but are not limited to municipal sewer system upgrades, broadband expansion, solar installations, and electric vehicle charging stations.

Sustainable Infrastructure makes an appearance in existing city documents in a variety of ways, including ensuring that transportation infrastructure provides a variety of mode choices, the large-scale goal to reduce emissions in the city, and ensuring that the city is prepared for the effects of climate change on future weather hazards and water use.

- **2021 Imagine Urbana Public Input** — Prepare for climate change, reduce environmental impact, and ensure that infrastructure is safe and accessible to all residents.
- **2020 Pedestrian Master Plan** — Improve Urbana’s pedestrian infrastructure to enable and encourage all residents and visitors to choose to walk to destinations.
- **2020-2024 Consolidated Plan** — To reduce utility costs and to improve the comfort of low-income families by incorporating energy conservation techniques into housing rehabilitation assistance.
- **2015-2020 Climate Action Plan** — 25% reduction in greenhouse gas emissions by 2020 from a 2007 baseline…80% reduction in greenhouse gas emissions by 2050 from a 2007 baseline.
- **2015 Hazard Mitigation Plan** — Reduce or eliminate potential losses by encouraging local policies that break the cycle of damage, reconstruction, and repeated damage of infrastructure. 
- **2013-2020 Sustainable Water Management Plan** — Protect surface water and stormwater quality; Plan for climate impacts on flood management…continue a decreasing trend of per capita potable water use.
- **2005 Comprehensive Plan** — Appropriately designed infill development will be encouraged to help revitalize the built urban environment, while new growth areas will be developed in a contiguous, compact and sustainable manner.

Outside of ARPA, the city has taken a variety of approaches to Sustainable Infrastructure, including the recently created Equity and Quality of Life fund, investing in alternative transportation, and encouraging individual initiatives like solar power and rain barrels. At the state and federal levels, investments included Illinois’s 2021 energy law, as well as the federal Bipartisan Infrastructure Law.

Example of other communities' ARPA infrastructure investments include the City of Champaign's plans to use around half of its funds toward infrastructure improvements in the Garden Hills neighborhood. Similarly, cities across the country are investing in water, sewer, and broadband updates, energy efficiency incentives, as well as investing in programs like regional food systems or creating local conservation corps.

## Public Health and Safety

Public Health and Safety includes a range of issues affecting the well-being of community members, including violence prevention, pandemic response, physical recreation, and safe public spaces. Investments using ARPA funds could include but are not limited to: vaccine outreach, domestic abuse response programs, youth engagement activities, and expansion of recreation spaces/opportunities.

Local plans reflect a variety of goals within this area, including mitigating violence, ensuring a fair criminal justice system, providing healthy home environments, and ensuring that residents can safely enjoy the outdoors and travel throughout the city.

- **2022-2023 Mayor-Council Goals** — Pursue methods to mitigate community violence. 
- **2021 Imagine Urbana Public Input** — Prevent violence; ensure that residents can walk, bike, and be outdoors safely; ensure fair treatment of all residents; and address the whole scope of health needs, including mental health.
- **2021 A Resolution Prioritizing Tactical De-Escalation in the Use of Force Policy and Police Transparency in Urbana**
- **2020-2024 HOME Consortium Consolidated Plan** — To remove unhealthy or hazardous housing conditions in low- and moderate-income areas 
- **2021-2022 City Budget** — Reflect the Urbana City Council’s commitment to ending structural racism and prioritizing de-escalation in police use of force.
- **2020 Pedestrian Master Plan** – Eliminate fatal and serious pedestrian/vehicle crashes.
- **2020 Park District Strategic Plan** – Create opportunities for wellness experiences open to all generations
- **2016 Bicycle Master Plan** — Enhance Safe Routes to Schools by installing bike routes and shared-use paths; Improve safety and continuity of existing bikeways and routes

Urbana and other local governments have taken a broad range of approaches to address local health and safety, including supporting local emergency responders, investing in de-escalation and crisis intervention, providing mental health support, and collaborating to address the need for testing and treatment in light of the COVID-19 pandemic.

These are also the types of programming that other cities are using their ARPA funds on—investing in local testing and vaccination, behavioral health, violence intervention, street safety, and other health and safety programs.

## Economic Recovery and Development

Economic Recovery and Development includes support to both businesses and individual workers —from business retention to workforce training—as well as providing the community infrastructure for workers and businesses to succeed. Investments using ARPA funds could include but are not limited to: grants to local businesses, job training for residents, and support for childcare and transportation to and from a job site or interview.

Plan objectives for economic development include support for existing businesses and individual workers, as well as recruitment of new businesses. In addition, several plans involve place-based investments, to make Urbana’s commercial areas more appealing and accessible and further develop local tax revenue.

- **2022-2023 Mayor-Council Goals** — Support current local businesses; Promote workforce development; Recruit new businesses and industries.
- **2021 Imagine Urbana Public Input** — Develop a more robust commercial tax base, support local and small-scale businesses, and support development, particularly for the commercial areas of downtown Urbana and the Philo Road corridor.
- **2016 Bicycle Master Plan** — Install bikeways to improve bike access to employers, especially major employers. 
- **2016 Tax Increment Financing Redevelopment Plan & Project** — Enhance the real estate tax base for the City and all overlapping taxing districts; Encourage and assist private investment in the redevelopment of the Area 
- **2012 Downtown Urbana Plan** — Strengthen economic activity in downtown Urbana.

Many scales of government—from the federal government to the City of Urbana—worked to support businesses during the pandemic, especially in its earliest phases. In addition to pandemic relief programs, local economic development initiatives include TIF and Enterprise Zones, downtown redevelopment projects, as well as ongoing programming from RPC, Champaign County EDC, and the Small Business Development Center.

ARPA economic development support in other cities includes funding for small businesses and neighborhood commercial districts, targeted support for specific businesses (like women or minority-owned), as well as subsidizing workforce training and support in a variety of fields, including sectors showing significant recent need, like healthcare and childcare.

## Adequate and Affordable Housing

Adequate and Affordable Housing refers to all community members having access to safe and healthy housing that they can afford. Investments using ARPA funds could include but are not limited to: rent/mortgage/utility assistance, emergency shelters, and transitional housing programs.

Documented goals in housing include supporting the quality of the housing stock, maintaining the security of residents in keeping their housing, and ensuring equal access. In addition, the city has stated a desire to revitalize neighborhoods with aging or lacking housing, and prepare the housing stock for future climate resiliency.

- **2022-2023 Mayor-Council Goals** — Support housing security and equity; Improve housing quality 
- **2021 Imagine Urbana Public Input** — Refurbish or redevelop older housing stock, create more mixed-income and integrated neighborhoods (including through small-scale multifamily development), and provide more support through subsidized housing and shelters for houseless residents.
- **2020-2024 Consolidated Plan** — To encourage the revitalization and stabilization of low- and moderate-income neighborhoods by implementing housing programs. 
- **2015-2020 Climate Action Plan** — Adapt to climate change impacts. 
- **2012 Downtown Urbana Plan** — Promote context-appropriate urban-style infill development to extend downtown’s core character…Increase downtown’s vitality by attracting more residents and visitors.

Similarly to the previous category, a variety of pandemic programs at various scales have addressed the immediate housing concerns, including additional HUD home funds, vouchers to the housing authority, and rental support programs. These have built on existing local supports, including the Housing Authority of Champaign County, home refurbishing and weatherization programs through RPC, Cunningham Township Assistance, and local service providers like C-U at Home.

Cities have used their ARPA funds for a variety of housing programming, including cash assistance to help residents make rent, mortgage, or utility payments, investments in constructing or acquiring facilities for public housing, emergency shelter, and transitional housing, as well as working with local housing stakeholders to help place voucher holders in housing.

## Human Rights and Social Services

Human Rights and Social Services refers to providing opportunities for all residents to thrive regardless of income, background, or personal attributes. Investments using ARPA funds could include but are not limited to: behavioral health programs, food banks, and reentry programs for those impacted by the criminal justice system.

Documents reflecting this priority include the 2021 resolution to end structural racism and achieve racial equity, as well as a variety of initiatives to ensure that all Urbana residents have access to needed resources and services, ensuring that all Urbana neighborhoods are safe, resilient, and enjoyable.

- **2021 Imagine Urbana Public Input** — Provide more engagement with and resources to historically excluded communities; provide the services that residents need to thrive.
- **2021 A Resolution Committing to End Structural Racism and Achieve Racial Equity**
- **2020-2024 HOME Consolidated Plan** — To enable low-income families who cannot obtain conventional financing to rehabilitate their homes 
- **2020 Pedestrian Master Plan** — Invest in Urbana’s pedestrian resources (infrastructure, education, encouragement, and enforcement) to improve all substandard areas, especially areas of concentrated racial or ethnic minorities, lower-income areas, and transit-dependent populations. 
- **2020 Urbana Park District Strategic Plan** — Advance efforts that make UPD parks and facilities more welcoming for everyone. 
- **2016 Bicycle Master Plan** — Install bikeways to improve bike access in low-income neighborhoods, especially in areas where bicycles may be a primary form of transportation for people.
- **2015 Hazard Mitigation Plan** — Improve the capability of participant populations to rapidly recover from disruption caused by natural hazards, adverse impacts associated with climate change, and technical hazards.

Locally, the City’s Office of Human Rights and Equity is working to realize these goals and is collaborating with a variety of partners, including Cunningham Township, Head Start programming, and various nonprofits, serving wide-ranging clientelle from immigrants to the formerly incarcerated, to ensure that all residents are being served.
  
ARPA programs addressing these needs in other communities include helping groups with particular barriers to accessing government services, enhancing existing service providers in light of the increased needs from the pandemic, and partnering with institutions like local school districts to meet residents where they are and address their needs.

