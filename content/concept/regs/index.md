---
title: "Federal ARPA Regulations"
draft: false
weight: 20
---


The summary of ARPA regulations below is based on the [ARPA SLFRF Final Rule]( https://home.treasury.gov/policy-issues/coronavirus/assistance-for-state-local-and-tribal-governments/state-and-local-fiscal-recovery-funds), which was released by the federal Treasury Department in January of 2022 and took effect on April 1, 2022.

These regulations guide the types of spending that are eligible for ARPA funds nationwide; however, the City of Urbana has worked to further focus its funds, as described in the [Urbana ARPA Funding Goals](https://ccrpc.gitlab.io/urbana-arpa/concept/afgs/#urbana-arpa-funding-goals-afgs) section on the previous page. In addition, as described in the [Introduction](https://ccrpc.gitlab.io/urbana-arpa/concept/afgs/), the city has already determined the size of its Local Revenue Replacement allocation as well as the sub-allocations uses within Local Revenue Replacement. Because of this, all spending from the $10 million allocated toward Urbana ARPA Funding Goals must meet one of the Eligible Uses below besides Local Revenue Replacement. 

## Eligible Uses

### Local Revenue Replacement

Revenue replacement funds may be broadly used for “any service traditionally provided by a government,” outside of Ineligible Uses that will be articulated later. These funds are intended to make up for a reduction in government revenue due to the COVID-19 pandemic. This is the most flexible use category, with streamlined reporting and compliance requirements.

SLFRF funding recipients have two choices for calculating how much of their funds can be spent on revenue replacement. The first is a Standard Allowance of up to $10 million, not to exceed that recipient’s actual award amount. The second choice is to use a formula that the Treasury Department created to compare actual annual revenue post-pandemic to a counterfactual growth trend. In the spring of 2022, the City of Urbana elected to take the Standard Allowance, and rather than taking the full $10 million, allocated $2.5 million toward this funding category. The city allocated this amount toward revenue replacement because of a Treasury Department-set deadline for declaring Revenue Replacement allocations, so no additional funds can be put toward this category. The introduction to this plan describes the programming already planned for these funds. For the remaining unallocated Revenue Replacement funds, non-exhaustive suggestions by the Treasury Department include:

- Construction of schools and hospitals
- Road building and maintenance, and other infrastructure
- Health services
- General government administration, staff, and administrative facilities
- Environmental remediation
- Provision of police, fire, and other public safety services (including the purchase of fire trucks and police vehicles)

### COVID-19 Public Health and Economic Impacts

Spending on public health and economic impacts from the COVID-19 pandemic can be broken into the following eligible categories, to be covered in the following section - 
- Public health
- Assistance to households
- Assistance to small businesses
- Assistance to nonprofits
- Aid to impacted industries
- Public sector capacity

The simple formula that the Treasury Department lays out to determine eligible public health and economic impact spending is to–

1.	Identify a COVID-19 public health or economic impact on an individual or class (i.e., a group), and
2.	Design a program that responds to that impact

#### Responding to the Public Health Emergency

Eligible use categories to respond to the public health emergency include
- COVID-19 mitigation and prevention
- Medical expenses
- Behavioral health care, such as mental health treatment, substance use treatment, and other behavioral health services
- Preventing and responding to violence

#### Responding to Negative Economic Impacts

For households/communities, small businesses, and nonprofits, the Treasury Department has compiled lists of groups that it considers “impacted”, as well as “disproportionately impacted” by the pandemic. “Impacted” groups are eligible for assistance to alleviate negative economic impact.  Programs serving “disproportionately impacted” groups allow for the greatest flexibility, using funds to address economic impacts in ways that aren’t eligible for simply “impacted groups”. Impacted groups generally suffered quantifiable harm from the pandemic (unemployment, food/housing insecurity, decreased revenue, financial insecurity). Disproportionately Impacted groups include low-income households/communities, as well as households, communities, small businesses, and nonprofits located in federal Qualified Census Tracts.

More information can be found in the Treasury Department’s Overview of the Final Rule, but in general - 
- Impacted groups are eligible for financial or service assistance (cash assistance; food assistance; housing assistance; paid leave; childcare; loans, grants, or technical assistance to businesses and nonprofits)
- Disproportionately impacted groups are additionally eligible for capital investments (property improvements, facility investments, commercial property rehabilitation), as well as resources for business start-ups

#### Aid to Impacted Industries

Industries that are eligible to receive aid under this category 
- Are in the travel, tourism, or hospitality sector,
- Experienced at least 8% employment loss from the pandemic, or
- Experienced quantifiably equal or worse impacts from the pandemic than the national travel, tourism, or hospitality industries

Aid in this category can only be provided to businesses that were operational before the pandemic and were affected by its spread and containment efforts. This aid should be broadly available to all businesses in the industry, and operational expenses should be given priority. Eligible aid types include mitigating financial hardship, technical assistance to support business planning, and COVID-19 mitigation and infection prevention.

#### Public Sector Capacity

Categories of eligible use under public sector capacity include - 
- Public Safety, Public Health, and Human Services Staff
- Government Employment and Rehiring Public Sector Staff
- Supporting and retaining public sector workers
- Effective Service Delivery

### Premium Pay for Essential Workers

Essential workers—in both the public and private sectors—are eligible for premium pay. This pay can either be provided directly to employees of the ARPA recipient government or as a grant to employers of eligible workers. 
Essential work must - 
- Involve in-person interactions or regularly handling items handled by others
- Be needed to maintain continuity of essential operations and infrastructure for the health and wellbeing of the community

Eligible sectors for essential work include – 
- Health care
- Emergency response
- Sanitation, disinfection & cleaning
- Maintenance
- Grocery stores, restaurants, food production, and food delivery
- Pharmacy
- Biomedical research
- Behavioral health
- Medical testing and diagnostics
- Home and community-based health care or assistance with activities of daily living
- Family or childcare
- Social services
- Public health
- Mortuary
- Critical clinical research, development, and testing necessary for COVID-19 response
- State, local, or Tribal government workforce
- Workers providing vital services to Tribes
- Educational, school nutrition, and other work required to operate a school facility
- Laundry
- Elections
- Solid waste or hazardous materials management, response, and cleanup

The ARPA recipient government has the option to add sectors to this list, with written justification for why this sector provided essential work during the pandemic.

Premium pay can pay up to $13 per hour in addition to the worker’s normal pay, not to exceed $25,000 per eligible worker. This use should prioritize lower-income workers; if premium pay increases a worker’s pay to more than 150% of the state average annual wage, the ARPA recipient must justify in writing why this increase is warranted. ARPA premium pay must be additive, rather than substituting for wages. It can be paid for current work or paid retroactively for work since the start of the pandemic. Retroactive premium pay should receive preference, to compensate workers for keeping these sectors running in the worst periods of the early pandemic.

If premium pay is provided as grants to private employers, then additional reporting is required.

### Investments in Water, Sewer, or Broadband Infrastructure

Water and sewer projects are eligible for ARPA funds if they are -
- Eligible under EPA’s Clean Water State Revolving Fund (CWSRF)
- Eligible under EPA’s Drinking Water State Revolving Fund (DWSRF)
- Otherwise deemed necessary to achieve or maintain an adequate minimum level of service

Broadband Projects are eligible for ARPA funds if they are -
- In an area lacking reliable or affordable high-speed broadband,
- Designed to meet high-speed technical standards, and
- Require the internet provider to enroll in a low-income subsidy program

In addition, projects to modernize cybersecurity for new or existing broadband infrastructure (of any speed) qualify for funding.

## Ineligible Uses
In addition to the Eligible Uses described above, the Final Rule also describes several uses that are specifically ineligible for ARPA SLFRF funds. These ineligible uses include - 
- Paying off pension obligations, debt service, or legal obligations from settlements or judgments
- Depositing into rainy day funds or reserves
- Implementing new tax cuts and using SLFRF funds to offset the lost revenue from these tax cuts
- Uses that violate or work against ARPA or other federal, state, and local laws

## ARPA Funding Timeline

Regarding spending timelines, the Final Rule states that ARPA SLFRF funds must be used to cover costs incurred by December 31, 2024. This means that all funding must be obligated by this time, but not that all funded activities must be completed (for example, ARPA-funded construction projects do not need to be fully built by this time). However, the ARPA period of performance runs until December 31, 2026, when ARPA-funded projects are required to be completed.

In addition, ARPA SLFRF funds can be used to cover costs incurred starting on March 3, 2021. The exception to this rule is for retroactive premium pay, which can go towards work beginning on January 27, 2020.

